#include <Windows.h>
#include <iostream>
#include <istream>
#include <stdio.h>
#include <tchar.h>
#include "Helper.h"
#include <thread>
#include <chrono>

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::wstring;
using std::vector;

#define BUF_SIZE 255

string getPath();
void pwd();
void cd(string);
void create(string);
void ls();
void secret();
void start(string);
DWORD WINAPI MyThreadFunction(LPVOID params);
void thread();

int main()
{
	string line;
	cout << "Not Windows [Version 1.0]\n(c)2018 Itay Levi inc. All rights reserved.\n" << endl;
	while (true)
	{
		cout << ">>";
		std::getline(cin,line);
		vector<string> argv = Helper::get_words(line);
		if (argv.size() > 0)
		{
			if (argv[0] == "pwd")
			{
				pwd();
			}
			else if (argv.size() == 2 && argv[0] == "cd")
			{
				cd(argv[1]);
			}
			else if (argv.size() == 2 && argv[0] == "create")
			{
				create(argv[1]);
			}
			else if (argv.size() == 1 && argv[0] == "ls")
			{
				ls();
			}
			else if (argv.size() == 1 && argv[0] == "secret")
			{
				secret();
			}
			else if (argv.size() == 2 && argv[0] == "start")
			{
				start(argv[1]);
			}
			else if (argv.size() == 1 && argv[0] == "thread")
			{
				thread();
			}
		}
	}
}

string getPath()
{
	DWORD dwRet;
	char path[MAX_PATH];
	dwRet = GetCurrentDirectoryA(MAX_PATH,path);
	if (dwRet == 0)
	{
		printf("GetCurrentDirectory failed (%d)\n", GetLastError());
		return nullptr;
	}
	if (dwRet > MAX_PATH)
	{
		printf("Buffer too small; need %d characters\n", dwRet);
		return nullptr;
	}
	return string(path);
}

void pwd()
{
	cout << getPath() << endl;
}

void cd(string path)
{
	DWORD dwRet;
	dwRet = SetCurrentDirectory(path.c_str());
	if (!dwRet)
	{
		printf("The system cannot find the path specified.\n");
		return;
	}
}

void create(string path)
{
	HANDLE h = CreateFile(TEXT(path.c_str()), GENERIC_READ, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	CloseHandle(h);
}

void ls()
{
	HANDLE hFind;
	WIN32_FIND_DATA data;

	hFind = FindFirstFile(string(getPath() + "\\*.*").c_str(), &data);
	if (hFind != INVALID_HANDLE_VALUE) {
		do 
		{
			printf("%s\n", data.cFileName);
		} 
		while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
}

void secret()
{
	HINSTANCE lib = LoadLibrary("SECRET.dll");
	FARPROC ProcAdd;

	if (lib != NULL)
	{
		ProcAdd = GetProcAddress(lib, "TheAnswerToLifeTheUniverseAndEverything");
		
		if (ProcAdd != NULL)
		{
			cout << ProcAdd() << endl;
		}
	}
	else
	{
		cout << "Could not load dll\n" << GetLastError() << endl;
	}
}

void start(string file)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if (!CreateProcess(NULL, const_cast<char *>(file.c_str()), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
	{
		cout << "The system cannot find the file " << file << endl;
	}
	else {

		WaitForSingleObject(pi.hProcess, INFINITE);

		DWORD exitCode;
		GetExitCodeProcess(pi.hProcess, &exitCode);
		cout << "The program exited with code " << exitCode << endl;
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);	
}

DWORD WINAPI MyThreadFunction(LPVOID params)
{
	cout << "started thread" << endl;
	std::this_thread::sleep_for(std::chrono::seconds(2));
	cout << "thread ended" << endl;
	return 0;
}

void thread()
{
	DWORD   dwThreadId;
	HANDLE  hThread;

	hThread = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		MyThreadFunction,       // thread function name
		NULL,          // argument to thread function 
		0,                      // use default creation flags 
		&dwThreadId);   // returns the thread identifier 

	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);

}